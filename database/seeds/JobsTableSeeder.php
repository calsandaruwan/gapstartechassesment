<?php

use Illuminate\Database\Seeder;

class JobsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $jobs = [
            'Software Engineer',
            'UI/UX Engineer',
            'QA Engineer',
            'Software Architecture',
            'Front end developer'
        ];

        foreach($jobs as $key=>$val){
            DB::table('jobs')->insert([
                'name' => $val,
                'created_at'=>\Carbon\Carbon::now(),
                'updated_at'=>\Carbon\Carbon::now()
            ]);
        }
    }
}
