<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
//            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {

        $user = User::create([
            'salutation' =>'',
            'f_name' => '',
            'l_name'=>'',
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'activated_at'=>null,
            'completed'=>0
        ]);

        //For the activation url, i'm using the USER ID as the identifier to reduce the complexity
        //This is not the best way, but in this case it is only focused on functionality and time

        if($user){
            Mail::send('email.activate', ['user' => $user,'url'=> URL::to('/profile/activate/'.$user->id)], function ($m) use ($user) {
                $m->from('sandaru.kaluarachchi@gmail.com', 'Lahiru');

                $m->to($user->email, $user->name)->subject('Activate Account');
            });
        }

        return $user;

    }
}
