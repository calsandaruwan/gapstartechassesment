<?php

namespace App\Http\Controllers\Web;

use Illuminate\Routing\Controller as BaseController;
use \App\User;
use \App\Job;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;


class ProfileController extends BaseController{

    private $_user = '';
    private $_job = '';

    public function __construct(){
        $this->_user = new user();
        $this->_job = new job();
    }

    public function activate($id){

        $user = $this->_user->find($id);

        if($user){
            if(!$user->activated_at) {
                $user->activated_at = date('Y-m-d H:i:s');
                $user->completed = 20;

                $save = $user->save();

            }

            return redirect('/#/profile/information?id='.$user->id);

        }
    }

    public function update($id){

        $user = $this->_user->find($id);

        if($user){
            $user->salutation = Input::get('salutation');
            $user->f_name = Input::get('f_name');
            $user->l_name = Input::get('l_name');
            $user->phone_number = Input::get('phone_number');
            $user->post_code = Input::get('post_code');
            $user->house_number = Input::get('house_number');
            $user->dob = Input::get('dob');
            $user->completed = 40;

            $save = $user->save();

            return json_encode($user);
        }

        return false;
    }

    public function getJobs(){
        $jobs = $this->_job->all();
        return json_encode($jobs);
    }

    public function setJobs($id){

        $user = $this->_user->find($id);

        $jobs = Input::get('ids');

        if($user && isset($jobs)){
            $user = $this->_user->find($id);
            $user->completed = 50;
            $user->save();
            $user->jobs()->sync($jobs);

            return json_encode($user);
        }

        return json_encode(['error'=>true]);

    }

    public function chart(){
        $users = DB::select('select onboarding_perentage , count(onboarding_perentage) as bording_percentage_count ,week(created_at,1) as week  from sample_users group by week, onboarding_perentage order by onboarding_perentage ASC');

        $week_array=[];
        foreach($users as $user){

            if(! is_null($user->onboarding_perentage)) {
                if (!isset($week_array[$user->week])) {
                    $week_array[$user->week] = [[0, 100,]];
                }
                $week_array[$user->week][] = [$user->onboarding_perentage, $user->bording_percentage_count,];
            }
        }
        return json_encode([
            'weeks'=>$week_array
        ]);
    }

}
