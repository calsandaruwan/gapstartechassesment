require('./bootstrap');

window.Vue = require('vue');
window.VueRouter = require('vue-router');
window.VueResource = require('vue-resource');
window.VueHighcharts = require('vue-highcharts');


Vue.component('navigation', require('./components/navigation/Navigation.vue'));

Vue.use(VueRouter);
Vue.use(VueResource);
Vue.use(VueHighcharts);

Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('content');

Vue.http.interceptors.push(function (request, next) {
    // request.headers['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('content');
    console.log(request.headers);
    next();
});


//vue router config
const routes = [
    {
        path: '/',
        component: require('./components/account/Login.vue')
    },
    {
        name:'login' ,
        path: '/login',
        component: require('./components/account/Login.vue')
    },{
        name:'register' ,
        path: '/register',
        component: require('./components/account/Register.vue')
    },{
        name:'chart' ,
        path: '/chart',
        component: require('./components/account/Chart.vue')
    },{
        name:'profile' ,
        path: '/profile',
        component: require('./components/account/UserProfile.vue'),
        children:[
            {
                name:'activate-account',
                path:'activate',
                component:require('./components/account/partials/Notice.vue')
            },{
                name:'profile-information',
                path:'information',
                component:require('./components/account/partials/Information.vue')
            },{
                name:'jobs',
                path:'jobs',
                component:require('./components/account/partials/Jobs.vue')
            },{
                name:'experience',
                path:'experience',
                component:require('./components/account/partials/Experience.vue')
            },{
                name:'freelance',
                path:'freelance',
                component:require('./components/account/partials/Freelance.vue')
            },{
                name:'approval',
                path:'approval',
                component:require('./components/account/partials/Notice.vue')
            }
        ]
    }
];

const router = new VueRouter({
    routes:routes
});

const app = new Vue({
    data: {
        sample: 'Hello there'
    },
    router:router
}).$mount('#temper');
