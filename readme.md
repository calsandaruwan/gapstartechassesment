## How to config

- Clone the repo
- Install dependancies with composer
- All configurations can be configured through .env file
- Setup a db with username and password
- Setup smtp mail. Provide host password and username (This step is important to send the verification link to the registered user)
- Run migrations
- Run seeders
- Install npm and run the start script

## Features

- not fully completed
- completed untill the registration of jobs
- back end with laravel
- front end with vue.js
- authentication is not completed yet
 
## urls

- homepage		{{baseurl}}/#/
- login			{{baseurl}}/#/login
- register    	{{baseurl}}/#/register
- profile indo	{{baseurl}}/#/profile/information?id={{id}}
- profile indo	{{baseurl}}/#/profile/jobs?id={{id}}
- chart			{{baseurl}}/#/chart