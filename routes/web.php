<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Auth routes
Auth::routes();

//welcome route
Route::get('/','web\WelcomeController@show')->name('welcome');
Route::get('/home', 'HomeController@index')->name('home');

//profile routes
Route::get('/profile/activate/{id}','web\ProfileController@activate')->name('activate_profile');
Route::post('/profile/update/{id}','web\ProfileController@update')->name('update_profile');
Route::get('/profile/jobs','web\ProfileController@getJobs')->name('get_jobs');
Route::post('/profile/jobs/{id}','web\ProfileController@setJobs')->name('set_jobs');

//chart
Route::get('/profile/chart','web\ProfileController@chart')->name('chart');
