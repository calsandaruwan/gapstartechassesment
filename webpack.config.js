/**
 * Created by lahiru on 10/6/16.
 */
var path = require('path');
module.exports = {
    entry:path.join(__dirname, 'resources', 'assets','js', 'app.js'),
    output:{
        path:path.join(__dirname, 'public', 'js'),
        filename:'bundle.js'
    },
    resolve: {
        alias: {
            vue: 'vue/dist/vue.js'
        }
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                loader: 'babel',
                exclude: /node_modules/
            },
            {
                test: /\.vue$/,
                loader: 'vue'
            },{
                test:/\.(svg|png|jpg|eot|woff|woff2|ttf)$/,
                loader:'url'
            },{
                test:/\.css$/,
                loaders:['style','css']
            }
        ],
        vue: {
            loaders: {
                js: 'babel'
            }
        }
    }
}